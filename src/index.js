import * as React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
// import First from './first/First';
import Home from './first/Home';
import About from './first/About';
import Contact from './first/Contact';
import * as serviceWorker from './serviceWorker';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';

//ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<First />, document.getElementById('root'));

const routing = (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/users">Users</Link>
          </li>
          <li>
            <Link to="/contact">Contact</Link>
          </li>
        </ul>
        <Route exact path="/" component={Home} />
        <Route path="/users" component={About} />
        <Route path="/contact" component={Contact} />
      </div>
    </Router>
  )

  ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
